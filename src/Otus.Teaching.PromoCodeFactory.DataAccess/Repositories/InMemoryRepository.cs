﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> AddEmployeeAsinc(T newEmployee)
        {
            Data = Data.Append(newEmployee);

            return Task.FromResult(newEmployee);
        }

        public Task<T> UpdateEmployeeAsinc(T updateEmployee)
        {
            List<T> employeesData = new List<T>();

            foreach (var item in Data)
            {
                if (item.Id == updateEmployee.Id)
                {
                    employeesData.Add(updateEmployee);
                }
                else
                {
                    employeesData.Add(item);
                }
            }
            Data = employeesData;

            return Task.FromResult(updateEmployee);
        }

        public Task<T> DeleteEmployeeAsinc(Guid id)
        {
            List<T> employeesData = new List<T>();

            foreach (var item in Data)
            {
                if (item.Id != id)
                {
                    employeesData.Add(item);
                }
            }
            Data = employeesData;

            return Task.FromResult(Data.FirstOrDefault());
        }
    }
}